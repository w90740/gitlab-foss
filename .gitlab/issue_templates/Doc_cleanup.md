<!--
* Use this template for documentation issues identified
* by [Vale](https://docs.gitlab.com/ee/development/documentation/testing.html#vale)
* or [markdownlint](https://docs.gitlab.com/ee/development/documentation/testing.html#markdownlint).
* This template is meant to describe work for first-time contributors or
* for work during Hackathons.
*
* Feature development work should not use this template. Use the Feature Request template instead.
-->

## Hi community contributors! :wave:

Do you want to work on this issue?

- **If the issue is assigned to someone already**, choose another issue.

- **If the issue is unassigned**, in a comment, type `@gl-docsteam I would like to work on this issue` and a writer will assign it to you.

## To resolve the issue

[Follow these instructions to create a merge request](https://docs.gitlab.com/ee/development/documentation/workflow.html#how-to-update-the-docs).

- Try to address the issue in a single merge request.
- Try to stick to the scope of the issue. If you see other improvements that can be made in the file, open a separate merge request.
- When you create the merge request, select the **Documentation** merge request description template.
- In the merge request's description, add a link to this issue.
- Follow the [commit message guidelines](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#commit-messages-guidelines).
  Use three to five words for your commit message, start with message with a capital letter, and do **not** end it in a period.
  Other commit messages can cause the pipeline to fail.

Thank you again for contributing to the GitLab documentation!

### A few additional notes

- If you were not assigned the issue, do not create a merge request. It will not be accepted.
- If this issue is for a Hackathon, merge requests created before the Hackathon has started
  do not count towards the Hackathon.

## Documentation issue

<!--
* Describe the issue. If the item is from an automated test,
* include a copy/paste from the the test results.
* [This issue](https://gitlab.com/gitlab-org/gitlab/-/issues/339543) is an example of text to include with a Vale issue.
*
* Limit the work to a reasonable amount. For example, you might have
* several moderate changes on one page, a few intermediate changes across five pages, or several very small
* changes for up to 10 pages. Break larger items into smaller issues to better distribute
* the opportunities for contributors.
*
* If you expect the work to take more than one MR to resolve, explain approximately
* how many MRs you expect to receive for the issue.
-->

## Additional information

<!--
* Any concepts, procedures, reference info we can add to make it easier to successfully use GitLab.
* Include use cases, benefits, and/or goals for this work.
* If adding content: What audience is it intended for? (What roles and scenarios?)
  For ideas, see personas at https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/ or the persona labels at
  https://gitlab.com/groups/gitlab-org/-/labels?subscribed=&search=persona%3A
-->

### Other links/references

<!-- For example, related GitLab issues/MRs -->

/label ~documentation
